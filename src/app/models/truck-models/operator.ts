import { Vehicle } from './vehicle';
import { Company } from './company';

export class Operator {
    id: string;
    firstname: string;
    lastname: string;
    registerDate: string;
    birthDate: string;
    driverLicenseFront: string;
    driverLicenseBack: string;
    vehicle: Vehicle;
    company: Company;
}