import { GroupCompany } from './group-company';
import { Vehicle } from './vehicle';


export class Company {
    id: string;
    name: string;
    operation: string;
    establishedDate: string;
    group: GroupCompany;
    hasStock: boolean;
}