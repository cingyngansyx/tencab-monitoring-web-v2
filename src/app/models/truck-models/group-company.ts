import { Company } from './company';


export class GroupCompany {
    id: string;
    name: string;
    establishedDate: string;
    companies: Array<Company>;
}