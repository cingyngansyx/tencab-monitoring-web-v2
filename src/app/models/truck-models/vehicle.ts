import { VehicleType } from './vehicle-type.enum';
import { Company } from './company';


export class Vehicle {
    accuracy: null;
    address: string;
    altitude: number;
    course: number;
    deviceId: string;
    deviceTime: string;
    fixTime: string;
    latitude: number;
    longitude: number;
    outdated: boolean
    positionId: string;
    protocol: string;
    serverTime: string;
    speed: number;
    valid: boolean;
}