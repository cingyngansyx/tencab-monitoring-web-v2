export enum VehicleType {
    TRUCK = "TRUCK",
    COMPACT = "COMPACT",
    BUS = "BUS",
    WAGON = "WAGON",
}
