import { BankAccount } from './bank-account';
import { UserRole } from '../enums/user/user-role.enum';
import { PositionData } from '../../location/model-class/position';

export class User {
    id: string;
    firstname: string;
    lastname: string;
    bankAccount: BankAccount;
    email: string;
    enabled: boolean;
    lastLocation: PositionData;
    phone: number;
    registerNumber: string;
    registeredDate: Date;
    role: UserRole;
    username: string;
    date: string;
    profileImage: "";
    review: number;
    paymentOption: any;
}