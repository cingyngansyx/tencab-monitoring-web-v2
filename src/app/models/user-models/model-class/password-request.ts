
export class PasswordRequest {
    id: string;
    phoneNumber: string;
    requestId: string;
    resetRequestDate: Date;
    success: boolean;
    verificationCode: string;
}