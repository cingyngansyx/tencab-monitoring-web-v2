import { DriverState } from '../../enums/driver/driver-state.enum';
import { Cab } from './cab';
import { User } from '../user';
import { BankAccount } from '../bank-account';
import { PositionData } from '../../../location/model-class/position';

export class Driver extends User {
    cab: Cab;
    driverState: DriverState;
    lastLocation: PositionData;
    bankAccount: BankAccount;
    driverPassportBackImage: "";
    driverPassportFrontImage: "";
    passportBackImage: "";
    passportFrontImage: "";
    profileImage: "";
    registerNumber: "";
    review: number;
}