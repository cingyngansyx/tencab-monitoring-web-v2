
export class Cab {
    id: string;
    licensePlateNumber: string;
    mark: string;
    model: string;
    engineCapacity: string;
    imagePath: string;
    importedDate: string;
    madeAt: string;
    vin: string; //* Арлын дугаар
}
