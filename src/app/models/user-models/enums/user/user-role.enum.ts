export enum UserRole {

    EMPLOYEE = 'EMPLOYEE',
    RIDER = 'RIDER',
    DRIVER = 'DRIVER',
}