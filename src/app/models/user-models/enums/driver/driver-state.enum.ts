export enum DriverState {
    OFFLINE = "OFFLINE",
    ONLINE = "ONLINE",
    READY = "READY",
    BUSY = "BUSY",
    ALL = "",
}
