export class FeedBackComment {
    description: string;
    id: string;
    writtenDate: String;
    writtenUserId: String;
}