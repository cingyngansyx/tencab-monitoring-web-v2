import { PositionData } from "./position";

export class LastLocationData extends PositionData {
    targetId: string;
    updatedtime: Date;
    accuracy: number;
    direction: number;
    speed: number;
    speedAccuracy: number;
    updatedTime: string;
}
