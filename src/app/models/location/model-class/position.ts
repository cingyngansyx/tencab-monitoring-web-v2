
export class PositionData {
    latitude: number;
    longitude: number;
}
