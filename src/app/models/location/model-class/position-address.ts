import { PositionData } from "./position";


export class PositionWithAddress {
    completedAddress: string;
    completedPosition: PositionData;
    endAddress: string;
    endPosition: PositionData;
    startedAddress: string;
    startedPosition: PositionData;
}