export class Chat{
    destinationUserId: String
    id: number
    image: String
    message: String
    receivedDate: String
    sentDate: String
    sourceUserId: String
    voice: String
}