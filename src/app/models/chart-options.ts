export class ChartOptions {
    title: string;
    chartArea: { width: string, height: string };
    width: number;
    height: number;
    backgroundColor: string;
    isStacked: boolean;
    is3D: boolean
    hAxis: {
        textStyle: {
            color: string;
        }
    };
    vAxis: {
        textStyle: {
            color: string;
        }
    };
    legend: {
        textStyle: {
            color: string;
        }
    };
    titleTextStyle: {
        color: string;
    }
    colors: Array<string>;
}