import { FeedBackComment } from './feedback-comment';

export class Feedback {
    comments: FeedBackComment;
    createdDate: string;
    createdUserId: string;
    description: string;
    hasSolved: boolean;
    id: string;
    targetUserId: string;
    tripId: string;
}