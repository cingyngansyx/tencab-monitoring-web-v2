import { PaymentType } from '../enums/payment-type.enum';
import { PaymentState } from '../enums/payment-state.enum';

export class Payment {
    paidAmount: number;
    paidDate: string;
    paymentType: PaymentType;
    status: PaymentState;
}