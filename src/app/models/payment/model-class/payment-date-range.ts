export enum PaymentDateRange {
    WEEK = "WEEK",
    MONTH ="MONTH",
    YEAR = "YEAR"
}