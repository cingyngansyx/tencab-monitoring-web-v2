import { Payment } from './payment';

export class PaymentDetail {
    billNo: string;
    id: string;
    payments: Array<Payment>;
    remainingValueToPay: number;
    totalPaid: number;
    tripOrderId: string;
    valueToPay: number;
}