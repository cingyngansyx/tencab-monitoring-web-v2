export class PaymentStatus {
    notPaidIncomes: PaymentIncome;
    paidIncomes: PaymentIncome;
    totalIncomes: PaymentIncome;
}
export class PaymentIncome {
    incomes: any;
    total: number;
}