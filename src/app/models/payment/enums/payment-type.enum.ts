export enum PaymentType {
    QPAY = 'QPAY',
    TENLEND = 'TENLEND',
    BANK_TRANSACTION = 'BANK_TRANSACTION',
    CASH = 'CASH'
}