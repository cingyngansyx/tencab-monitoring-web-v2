export enum PaymentState {
    PAID = 'PAID',
    NOT_PAID = 'NOT_PAID',
}