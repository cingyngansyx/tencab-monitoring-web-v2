import { Chat } from "./chat";

export class Conversation {
	id:String
    orderId:String;
    chats: Array<Chat>
}