export class Page<T>{
    content: Array<T>
    totalElements: number
    totalPages: number
    numberOfElements: number
    number: number
    size: number
}