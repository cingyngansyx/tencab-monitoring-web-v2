export enum FormatType {
    STATE = "STATE",
    TYPE = "TYPE",
    COLOR = "COLOR",
    ICON = "ICON",
    ROLE = "ROLE",
}
