export enum DateType {
    DDMMYYYY = "DD-MM-YYYY",
    MMYYYY = "MM-YYYY",
    YYYY = "YYYY",
    DDMM = "MM-DD"
}
