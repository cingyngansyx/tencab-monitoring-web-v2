import { Conversation } from '../../conversation'
import { LastLocationData } from '../../location/last-location';
import { Order } from './order';

export class TripDetail {
    conversation: Conversation;
    cooridinatesBeforeTakeRider: Array<LastLocationData>;
    cooridinatesHasRider: Array<LastLocationData>;
    order: Order;
}