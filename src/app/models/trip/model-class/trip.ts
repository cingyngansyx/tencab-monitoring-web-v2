import { LocationAddress } from '../../location/location-address'
import { PaymentDetail } from '../../payment/payment-detail';
import { Driver } from '../../user-models/driver';
import { User } from '../../user-models/user';
import { Order } from '../../order/model-class/order';

export class Trip {
    address: LocationAddress;
    order: Order;
    payment: PaymentDetail;
    targetUser: Driver | User;
}