import { TripDetail } from './trip-detail';
import { LocationAddress } from '../../location/location-address';


export class TripRouteData {
    locationData: LocationAddress;
    routeData: TripDetail
}