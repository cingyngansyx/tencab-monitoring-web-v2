import { LocationAddress } from '../../location/location-address';
import { OrderState } from '../enums/order-state.enum';
import { OrderType } from '../enums/order-type.enum';

export class Order {
    cancelledDate: Date;
    driverId: Date;
    address: LocationAddress;
    feedbackId: string;
    hasCancelled: boolean;
    id: string;
    nearestRouteLength: number;
    orderedDate: string;
    paymentId: string;
    plannedDate: Date;
    priceForPerKm: number;
    riderId: string;
    routeLength: number;
    state: OrderState;
    type: OrderType;
}