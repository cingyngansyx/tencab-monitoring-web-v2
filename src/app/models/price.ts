import { TripType } from './enums/trip-type.enum'


export class Price {
    id: string;
    orderType: TripType;
    price: number;
    updatedDate: string;
}